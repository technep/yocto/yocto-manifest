# How to build Yocto-Docker image

```bash
# DEFAULT VARIABLES 
# MANIFEST_URI ?= "https://gitlab.com/technep/yocto/yocto-manifest.git"
# MANIFEST_TAG ?= "dunfell"
# For custom manifest use Read https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md
# make MANIFEST_URI="url/to/default.xml" MANIFEST_TAG="tag|branch-name" docker_build

make docker_build

```

# How to run Yocto-Docker image
```bash
make run
```

# yocto-manifest

