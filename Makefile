mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(dir $(mkfile_path))

DOCKER := $(shell which docker)
DOCKER_IMAGE ?= "technep/yocto:latest"

CONTAINER_WORK_DIR ?= "/home/developer/yocto"
MANIFEST_URI ?= "https://gitlab.com/technep/yocto/yocto-manifest.git"
MANIFEST_TAG ?= "dunfell"

run:
	@echo "MAKEFILE DIRECTORY $(current_dir)"
	@echo "CONTAINER WORKING DIR $(CONTAINER_WORK_DIR)"
	$(DOCKER) run -it --rm  \
	-v $(current_dir)sources:$(CONTAINER_WORK_DIR)/sources \
	-v $(current_dir)output:$(CONTAINER_WORK_DIR)/output $(DOCKER_IMAGE) bash

docker_build:
	cd "$(current_dir)/Docker" && \
	$(DOCKER)  build   \
	--build-arg "host_uid=$(shell id -u)"   \
	--build-arg "host_gid=$(shell id -g)"   \
	--build-arg "yocto_manifest_uri=$(MANIFEST_URI)"   \
	--build-arg "yocto_manifest_tag=$(MANIFEST_TAG)"   \
	--tag "$(DOCKER_IMAGE)" .
